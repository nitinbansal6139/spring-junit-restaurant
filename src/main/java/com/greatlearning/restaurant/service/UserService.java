package com.greatlearning.restaurant.service;

import org.springframework.stereotype.Service;

import com.greatlearning.restaurant.entity.User;

@Service
public interface UserService {

	String createUser(User user);
	
	String updateUser(User user);
	
	String deleteUser(int id);
	
	User fetchUser(int id);
}
