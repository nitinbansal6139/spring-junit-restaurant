package com.greatlearning.restaurant.service.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.greatlearning.restaurant.entity.FinancialReport;
import com.greatlearning.restaurant.entity.Item;
import com.greatlearning.restaurant.model.SelectItem;
import com.greatlearning.restaurant.repository.FinancialReportRepository;
import com.greatlearning.restaurant.repository.ItemDetailsRepository;
import com.greatlearning.restaurant.service.RestaurantItemService;
import com.greatlearning.restaurant.userdetails.CustomUserDetails;

@Service
public class RestaurantItemServiceImpl implements RestaurantItemService {

	@Autowired
	FinancialReportRepository financialReportRepository;

	@Autowired
	ItemDetailsRepository itemDetailsRepository;

	@Autowired
	EntityManager entityManager;

	@Override
	public List<Item> viewItemDetails() {
		return itemDetailsRepository.findAll();
	}

	@Override
	public String selectItemDetailsByIds(List<SelectItem> selectItem) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetails user = null;
		if(auth != null) {
			user =  (CustomUserDetails)auth.getPrincipal();
		}
		
		TypedQuery<Integer> maxBillIdQuery = entityManager
				.createQuery("Select max(f.billId) from FinancialReport f", Integer.class);
		int billId;
		List<Integer> resultList = maxBillIdQuery.getResultList();
		if(resultList.isEmpty()) {
			billId=1;
		}
		else {
			Integer maxBillId = resultList.get(0);
			if(maxBillId != null)
				billId=maxBillId+1;
			else
				billId=1;
		}
		Optional<Item> itemDetails;
		for (int i = 0; i < selectItem.size(); i++) {
			FinancialReport financialReport = new FinancialReport();
			itemDetails = itemDetailsRepository.findById(selectItem.get(i).getItemId());
			financialReport.setItemName(itemDetails.get().getName());
			
			financialReport.setUsername(user != null ?user.getFullName():null);
			financialReport.setBillId(billId);
			financialReport.setPurchaseDate(new java.sql.Date(System.currentTimeMillis()));
			financialReport.setQty(selectItem.get(i).getQty());
			financialReport.setPrice(itemDetails.get().getPrice() * selectItem.get(i).getQty());
			financialReportRepository.saveAndFlush(financialReport);
		}
		return "Items selected";
	}

	@Override
	public List<FinancialReport> viewFinalBill() {

		//List<FinancialReport> listAll = financialReportRepository.findAll(Sort.by(Direction.DESC, "billId"));
		TypedQuery<Integer> maxBillIdQuery = entityManager
				.createQuery("Select max(f.billId) from FinancialReport f", Integer.class);
		int billId;
		List<Integer> resultList = maxBillIdQuery.getResultList();
		if(resultList.isEmpty()) {
			billId=0;
		}
		else {
			Integer maxBillId = resultList.get(0);
			if(maxBillId != null)
				billId=maxBillId+1;
			else
				billId=0;
		}
		if(billId != 0) {
			//int maxId = listAll.get(0).getBillId();
			FinancialReport finalBill = new FinancialReport();
			finalBill.setBillId(billId);
			ExampleMatcher exampleMatcher = ExampleMatcher.matching()
					.withMatcher("finalcialReportToday", ExampleMatcher.GenericPropertyMatchers.exact())
					.withIgnorePaths("id","username", "itemName", "qty", "price", "purchaseDate");
			Example<FinancialReport> example = Example.of(finalBill, exampleMatcher);
			return financialReportRepository.findAll(example);
		}
		return null;
	}

	@Override
	public List<FinancialReport> viewDailyReport() {
		List<FinancialReport> reports = financialReportRepository.findTodaysPurchases();
		return reports;
	}

	@Override
	public List<FinancialReport> totalMonthlySale() {
		LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		TypedQuery<FinancialReport> purchasesFromThisMonth = entityManager
				.createQuery("Select l from FinancialReport l where extract(month from l.purchaseDate)=" + month
						+ " and extract(year from l.purchaseDate)=" + year, FinancialReport.class);
		return purchasesFromThisMonth.getResultList();
	}

}
