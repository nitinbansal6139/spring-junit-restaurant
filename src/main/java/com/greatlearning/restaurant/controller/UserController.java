package com.greatlearning.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.restaurant.entity.User;
import com.greatlearning.restaurant.service.UserService;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("")
	public ModelAndView viewHomePage() {
		ModelAndView view = new ModelAndView("signup_form");
		view.addObject("user", new User());
		return view;
	}

	@PostMapping("/process_register")
	public ModelAndView processRegister(User user) {
		user.setRole("USER");
		userService.createUser(user);
		ModelAndView view = new ModelAndView("register_success");
		return view;
	}
}